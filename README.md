# Welcome to the Coral Senior SRE Take Home Data Challenge

Thank you for applying for the Senior SRE role! We’ve created this take home data challenge to give you a taste of what it’s like working with the Coral application and it’s MongoDB database. The purpose of this challenge is to get you oriented with Coral’s application stack, and familiar with it’s internal objects and their properties.


## About the Task

We’ve estimated that this should take you between 1-2 hours to complete. Please do not spend more than 2 hours MAX on this challenge! If you run into issues and are unable to complete the challenge in that timeframe, instead of spending additional time please make notes as to what issues you encountered for us to discuss at the technical interview. 

Coral by Vox Media source code is publicly available on GitHub. For this challenge, you’ll need to use Docker to get a local installation of Coral up and running. Then you’ll use MongDB to import some json data into your local instance of Coral’s MongoDB. Once you’ve completed the import you should be able to answer the Data Questions listed at the end of this ReadMe. During the technical interview, we’ll review your answers to the Data Questions, as well as your local installation of Coral, so please plan to keep your local environment in the completed state for the technical interview. 

Each step of the challenge is a prerequisite for the remaining steps. This means that you’ll need to complete each step fully before moving on to the next. If you get stuck, go back, and make sure you really completed the previous steps. 


## What you will need

Docker [https://docs.docker.com/install/](https://docs.docker.com/install/) and Docker-compose [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/) 

Coral by Vox Media (Application Repo) [https://github.com/coralproject/talk](https://github.com/coralproject/talk) 

Coral’s Documentation [https://docs.coralproject.net/](https://docs.coralproject.net/) 

(Optional) Mongo Shell [https://docs.mongodb.com/manual/mongo/](https://docs.mongodb.com/manual/mongo/) 


## Instructions


### Step 1- Get Coral running locally

Using the steps outlined here [https://docs.coralproject.net/#docker](https://docs.coralproject.net/#docker) (with Docker-compose) launch running containers of Coral, Mongo, and Redis. There is a compatibility issue with Mac Catalina OS and Coral's version of Node, which you will bypass by using the docker compose instead of trying to build and run Coral locally. This step is completed when you have 3 containers running locally, and can access the Coral Installation wizard at [http://localhost:3000](http://localhost:3000). You should see this: 




![alt_text](installWizard.png "image_tooltip")



### Step 2 - Configure a Coral tennant

Click “GET STARTED” on the installation wizard to finish setting up Coral. You will be prompted to enter Organization details, and to create an Administrator user account.  You will need to remember the Admin login details you used to create the Administrator user account. At the prompt for “Site permitted Domains” enter the following: http://localhost.com.

Using mongo shell connect to Coral’s MongoDB container and determine the Tenant ID and Site ID that were created during setup. Locate the `site` document that was created (*hint: it’s in the “Sites” collection*), and note it’s `id` and `tenantID` values. You will need these UUIDs in the next step. 	


### Step 3 Transform & Import JSON Data

You’ve been provided with 3 json data files. In this step you’ll import this data to your locally running MongoDB database. However first you’ll need to modify the files. Use your favorite tool for manipulating text files to update the `tenantID` and `siteID` values on the json files with the values from your local installation. 

Then use mongoimport to import the data to Coral. For best results, import in the following order: Users, Stories, Comments. Some import errors are to be expected, and you do not need to try to resolve them. After the import is completed, you will need to run the following script in mongo shell, drop your TenantID into this script and execute:

```
db.users.updateMany(

	{	tenantID:"<tenantID>",

	role:"COMMENTER"

	},

	{

		$currentDate: {

			createdAt: true

		}

	})
```

If the script executed successfully, you should see ```{ "acknowledged" : true, "matchedCount" : 500, "modifiedCount" : 500 }```. 


Next we're going to update the comments collection as well.
```
db.comments.updateMany(

    {   tenantID:"<tenantID>"   },

    {

        $currentDate: {

            createdAt: true

        }

    })
```
If the script executed successfully, you should see ```{ "acknowledged" : true, "matchedCount" : 7226, "modifiedCount" : 7226 }```


If you do, you’ve completed the take home data challenge! Congratulations! 

Next we’re going to ask you some questions about the data in your local instance of Coral. 


## Data Questions



1. The script that was run on the users collection at the end of step 3, what does this script do and why might this script be necessary after a data import?
2. Navigate to “admin/dashboard” to answer the following questions:
    1. How many New comments?
    1. How many New community members?
    1. What is the title of Today’s most commented story?
    1. How many comments were imported to Today’s least commented story?
3. Navigate to Stories tab. How many comments does each story show as “Published”?
    1. Does this amount differ from the “Today’s most commented stories” numbers on the Dashboard?  If yes, what do you think could be causing that difference?
4. Navigate to the Community tab to answer the following questions:
    1. Does the User list scroll, or is it limited to a single page of users?
    1. Locate the user with the email address `omarimorissette@meda.info`, how many comments were imported for this user?
    1. Locate the user with the username `Mona_Kub`, what is this user’s email address?
    1. Locate the user with userID `e4f8764f-2ceb-45a4-a3ef-f1824561d11d`:
        1. How many comments were imported for this user?
        1. What is this user’s username & email address?